﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CaoAutoShutdown
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _timer;
        int ms = 0;
        private delegate void myDelegate(string str);//声明委托
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnPause.Enabled = false;
            btnReset.Enabled = false;
            _timer = new System.Timers.Timer(1000);
            _timer.Elapsed += _timer_Elapsed;
            _timer.AutoReset = true;
            numberHour_ValueChanged(null, null);
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            gpSetting.Enabled = false;
            BtnStart.Enabled = false;
            btnPause.Enabled = true;
            btnReset.Enabled = false;
            _timer.Start();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            gpSetting.Enabled = false;
            BtnStart.Enabled = true;
            btnPause.Enabled = false;
            btnReset.Enabled = true;
            _timer.Stop();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gpSetting.Enabled = true;
            BtnStart.Enabled = true;
            btnPause.Enabled = false;
            btnReset.Enabled = true;

            numberHour_ValueChanged(null,null);
        }

        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ms = ms - 1000;
            setCountDown(msToString(ms));

            if (ms <= 0)
            {
                _timer.Stop();

                setCountDown("正在关机..");

                //关机。。。。
                SystemUtil.PowerOff();
            }
        }

        private void setCountDown(string str)
        {
            //lbLog 控件名
            if (this.lblCountDown.InvokeRequired)
            {
                myDelegate md = new myDelegate(setCountDown);
                this.Invoke(md, new object[] { str });
            }
            else
                this.lblCountDown.Text = str;
        }

        private void numberHour_ValueChanged(object sender, EventArgs e)
        {
            var h = (int)numberHour.Value;
            var m = (int)numberMinute.Value;
            ms = (h * 3600 + m * 60) * 1000;
            setCountDown(msToString(ms));
            BtnStart.Enabled = ms > 0;
            
        }

        private void numberMinute_ValueChanged(object sender, EventArgs e)
        {
            var h = (int)numberHour.Value;
            var m = (int)numberMinute.Value;
            ms = (h * 3600 + m*60) * 1000;
            setCountDown(msToString(ms));
            BtnStart.Enabled = ms > 0;
        }

        private string msToString(int ms)
        {
            var totalSeconds = ms / 1000;
            var h = totalSeconds / 3600;
            var m = (totalSeconds - (h * 3600)) / 60;
            var s = totalSeconds - (h*3600)-(m*60);
            return h.ToString("D2") + ":" + m.ToString("D2") + ":" + s.ToString("D2");
        }
    }
}
