﻿
namespace CaoAutoShutdown
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.numberHour = new System.Windows.Forms.NumericUpDown();
            this.numberMinute = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gpSetting = new System.Windows.Forms.GroupBox();
            this.BtnStart = new System.Windows.Forms.Button();
            this.lblCountDown = new System.Windows.Forms.Label();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numberHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberMinute)).BeginInit();
            this.gpSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // numberHour
            // 
            this.numberHour.Location = new System.Drawing.Point(71, 28);
            this.numberHour.Name = "numberHour";
            this.numberHour.Size = new System.Drawing.Size(120, 25);
            this.numberHour.TabIndex = 0;
            this.numberHour.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberHour.ValueChanged += new System.EventHandler(this.numberHour_ValueChanged);
            // 
            // numberMinute
            // 
            this.numberMinute.Location = new System.Drawing.Point(71, 59);
            this.numberMinute.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numberMinute.Name = "numberMinute";
            this.numberMinute.Size = new System.Drawing.Size(120, 25);
            this.numberMinute.TabIndex = 1;
            this.numberMinute.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numberMinute.ValueChanged += new System.EventHandler(this.numberMinute_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "小时";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "分钟";
            // 
            // gpSetting
            // 
            this.gpSetting.Controls.Add(this.label1);
            this.gpSetting.Controls.Add(this.label2);
            this.gpSetting.Controls.Add(this.numberHour);
            this.gpSetting.Controls.Add(this.numberMinute);
            this.gpSetting.Location = new System.Drawing.Point(12, 12);
            this.gpSetting.Name = "gpSetting";
            this.gpSetting.Size = new System.Drawing.Size(200, 100);
            this.gpSetting.TabIndex = 3;
            this.gpSetting.TabStop = false;
            this.gpSetting.Text = "关机时间设置";
            // 
            // BtnStart
            // 
            this.BtnStart.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnStart.Location = new System.Drawing.Point(8, 118);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(69, 53);
            this.BtnStart.TabIndex = 4;
            this.BtnStart.Text = "开始";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // lblCountDown
            // 
            this.lblCountDown.AutoSize = true;
            this.lblCountDown.Font = new System.Drawing.Font("宋体", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblCountDown.Location = new System.Drawing.Point(27, 193);
            this.lblCountDown.Name = "lblCountDown";
            this.lblCountDown.Size = new System.Drawing.Size(151, 33);
            this.lblCountDown.TabIndex = 5;
            this.lblCountDown.Text = "10:10:00";
            // 
            // btnPause
            // 
            this.btnPause.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPause.Location = new System.Drawing.Point(83, 118);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(64, 53);
            this.btnPause.TabIndex = 6;
            this.btnPause.Text = "暂停";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnReset.Location = new System.Drawing.Point(153, 118);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(59, 53);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "重置";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 233);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.lblCountDown);
            this.Controls.Add(this.BtnStart);
            this.Controls.Add(this.gpSetting);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "曹老师的定时关机";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numberHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberMinute)).EndInit();
            this.gpSetting.ResumeLayout(false);
            this.gpSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numberHour;
        private System.Windows.Forms.NumericUpDown numberMinute;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gpSetting;
        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.Label lblCountDown;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnReset;
    }
}

